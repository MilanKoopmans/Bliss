window.onload = function() {
  GiveTags();
  SlideDown();
  GoScroll();
}

//====================================== Apply tags for responsiveness ======================================
function GiveTags() {
  var x = document.getElementById("nav");
  if (x.className === "NavigationBar") {
    x.className += " responsive";
  } else {
    x.className = "NavigationBar";
  }
}
//====================================== Apply tags for responsiveness ======================================
//==================================== Check if div is halfway into view ====================================

//==================================== Check if div is halfway into view ====================================

//====================================== Scroll onclick (NavigationBar)======================================
function GoScroll() {
  for(var i = 0; i < document.getElementsByClassName("DivPosition").length; i++) {
    var element = document.getElementsByClassName("DivPosition");
    element[i].onclick = function() {
      var destination = document.getElementById(this.dataset.position);
      var duration = 500;

      scrollIt(destination, duration, 'easeInOutQuart');
      setTimeout(function() { cancelAnimationFrame(scrollAnim); }, duration);
    }
  }
}

//====================================== Scroll onclick (NavigationBar)======================================

function scrollIt(destination, duration, easing) {
  var easings = {
    easeInOutQuart(t) { return t < 0.5 ? 8 * t * t * t * t : 1 - 8 * (--t) * t * t * t; }
  };
  var start = window.pageYOffset;
  var startTime = 'now' in window.performance ? performance.now() : new Date().getTime();
  var documentHeight = Math.max(document.body.scrollHeight, document.body.offsetHeight, document.documentElement.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight);
  var windowHeight = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName('body')[0].clientHeight;
  var destinationOffset = typeof destination === 'number' ? destination : destination.offsetTop;
  var destinationOffsetToScroll = Math.round(documentHeight - destinationOffset < windowHeight ? documentHeight - windowHeight : destinationOffset);


  if ('requestAnimationFrame' in window === false) {
    window.scroll(1, destinationOffsetToScroll);
  }

  function scroll() {
    var now = 'now' in window.performance ? performance.now() : new Date().getTime();
    var time = Math.min(1, ((now - startTime) / duration));
    var timeFunction = easings[easing](time);
    window.scroll(0, Math.ceil((timeFunction * (destinationOffsetToScroll - start)) + start));

    scrollAnim = requestAnimationFrame(scroll);
  }
    scroll();
}
//====================================== scroll function that I found online, Heavily modified to suit my needs ======================================

/* ==========function for sliding the navigation bar down, I made some adjustments to calculate the heightinstead of just blatandly throwing it into the code, for responsiveness' sake. ========== */
function SlideDown() {
  var el = document.getElementsByClassName("NavBarLink");
  var elPadTop = window.getComputedStyle(el[2], null).getPropertyValue('padding-top');
  var elPadBot = window.getComputedStyle(el[2], null).getPropertyValue('padding-bottom');
  var elHeight = window.getComputedStyle(el[2], null).getPropertyValue('height');
  var totalElHeight = parseFloat(elPadTop.slice(0, -2)) + parseFloat(elPadBot.slice(0, -2)) + parseFloat(elHeight.slice(0, -2));
  var minheight = totalElHeight;
  var maxheight = totalElHeight * el.length;
  var time = 150;
  var timer = null;
  var toggled = false;
  var controler = document.getElementById('icon');
  var slider = document.getElementById('nav');

  slider.style.height = minheight + 'px';

  controler.onclick = function() {
    clearInterval(timer);
    var instanceheight = parseInt(slider.style.height);
    var init = (new Date()).getTime();
    var height = (toggled = !toggled) ? maxheight: minheight;
    var disp = height - parseInt(slider.style.height);
    timer = setInterval(function() {
      var instance = (new Date()).getTime() - init;
      if(instance < time ) {
        var con = instance / time;
        var pos = Math.floor(disp * con);
        result = instanceheight + pos;
        slider.style.height =  result + 'px';
      }
      else {
        slider.style.height = height + 'px'; //safety side ^^
        clearInterval(timer);
        controler.value = toggled ? ' Slide Up ' :' Slide Down ';
      }
    },1);
  };
};
/* ==========function for sliding the navigation bar down, I made some adjustments to calculate the heightinstead of just blatandly throwing it into the code, for responsiveness' sake. ========== */
